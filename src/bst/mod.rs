pub mod tree;

use std::cmp::Ordering;

#[derive(Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct BSTNode<T: Ord> {
    pub left: Option<Box<BSTNode<T>>>,
    pub right: Option<Box<BSTNode<T>>>,
    pub key: T,
}

impl<'a, T: Ord> BSTNode<T> {
    fn new(key: T) -> BSTNode<T> {
        BSTNode {
            left: None,
            right: None,
            key,
        }
    }

    fn insert(&mut self, key: T) -> &BSTNode<T> {
        let target_node;
        match key.cmp(&self.key) {
            Ordering::Less | Ordering::Equal => target_node = &mut self.left,
            Ordering::Greater => target_node = &mut self.right,
        }
        if let Some(node) = target_node {
            return node.insert(key);
        } else {
            *target_node = Some(Box::new(BSTNode::new(key)));
            return target_node.as_deref().unwrap();
        }
    }

    fn search(&self, key: T) -> Option<&BSTNode<T>> {
        if self.key == key {
            return Some(self);
        }

        let target_node;
        match key.cmp(&self.key) {
            Ordering::Less | Ordering::Equal => target_node = &self.left,
            Ordering::Greater => target_node = &self.right,
        }
        if let Some(node) = target_node {
            return node.search(key);
        } else {
            return None;
        }
    }

    fn depth(&self) -> u128 {
        let left_depth;
        if let Some(ref left) = self.left {
            left_depth = Some(left.depth());
        } else {
            left_depth = None;
        }

        let right_depth;
        if let Some(ref right) = self.right {
            right_depth = Some(right.depth());
        } else {
            right_depth = None;
        }

        if left_depth == None && right_depth == None {
            return 0;
        }

        let left_depth = left_depth.unwrap_or(0);
        let right_depth = right_depth.unwrap_or(0);

        if left_depth > right_depth {
            return left_depth + 1;
        } else {
            return right_depth + 1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::BSTNode;

    #[test]
    fn test_node() {
        let mut node1 = BSTNode::new(1);
        let node2 = BSTNode::new(2);
        let node3 = BSTNode::new(3);
        node1.left = Some(Box::new(node2));
        node1.right = Some(Box::new(node3));
        // BSTNode 1
        let left_node = node1.left.unwrap();
        let right_node = node1.right.unwrap();
        assert_eq!(1, node1.key);
        assert_eq!(2, left_node.key);
        assert_eq!(3, right_node.key);
        // BSTNode 2
        assert_eq!(None, left_node.left);
        assert_eq!(None, left_node.right);
        // BSTNode 3
        assert_eq!(None, right_node.left);
        assert_eq!(None, right_node.right);
    }
}
