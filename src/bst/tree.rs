use super::BSTNode;
use std::cmp::Ordering;

pub struct BSTree<T: Ord> {
    root: Option<Box<BSTNode<T>>>,
}

impl<'a, T: Ord> BSTree<T> {
    pub fn new(root: T) -> BSTree<T> {
        BSTree {
            root: Some(Box::new(BSTNode::new(root))),
        }
    }

    pub fn empty() -> BSTree<T> {
        BSTree { root: None }
    }

    pub fn insert(&mut self, key: T) -> &BSTNode<T> {
        if let Some(ref mut node) = self.root {
            return node.insert(key);
        } else {
            self.root = Some(Box::new(BSTNode::new(key)));
            return self.root.as_deref().unwrap();
        }
    }

    pub fn delete(&self, key: T) -> bool {
        // TODO search first
        false
    }

    pub fn search(&mut self, key: T) -> Option<&BSTNode<T>> {
        if let Some(ref node) = self.root {
            return node.search(key);
        } else {
            return None;
        }
    }

    pub fn depth(&self) -> Option<u128> {
        if let Some(ref node) = self.root {
            return Some(node.depth());
        } else {
            return None;
        }
    }

    pub fn traversal() {
        // TODO with iterator?
    }
}

#[cfg(tests)]
mod tests {
    #[test]
    fn test_insert() {
        // TODO implement tests
    }

    #[test]
    fn test_delete() {
        // TODO implement tests
    }

    #[test]
    fn test_search() {
        // TODO implement tests
    }

    fn test_depth() {
        // TODO implement tests
    }
}
