pub mod bst;

#[cfg(test)]
mod tests {
    use super::bst::tree::BSTree;
    #[test]
    fn it_works() {
        let mut x = BSTree::new(1);
        x.insert(1);
        let y = x.search(1);
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
